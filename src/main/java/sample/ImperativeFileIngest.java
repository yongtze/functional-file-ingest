package sample;

import sample.model.Contact;
import sample.model.FileIngestResult;
import sample.model.ValidationError;
import sample.util.ContactDb;
import sample.util.ContactFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

public class ImperativeFileIngest {

    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Please specify the required filename parameter to run this program.");
            System.exit(1);
            return;
        }

        final FileIngestResult result = new ImperativeFileIngest().ingestFile(new File(args[0]));

        System.out.println("File");
    }

    private FileIngestResult ingestFile(final File file) {
        long lineNumber = 0;
        long totalRowsIngested = 0;
        long totalErrorRows = 0;
        List<ValidationError> allValidationErrors = new ArrayList<>();
        try (
                final BufferedReader reader = new BufferedReader(new FileReader(file));
                final Connection connection = ContactDb.createConnection();
                final PreparedStatement insertStmt = ContactDb.prepareInsertStatement(connection);
        ) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                lineNumber++;
                if (lineNumber == 1) {
                    // Ignore the header line.
                    continue;
                }

                final String[] tuple = line.split("\\t");
                final Contact contact = ContactFile.mapRow(tuple);
                final List<ValidationError> validationErrors = ContactFile.validateContact(lineNumber, contact);

                if (validationErrors.isEmpty()) {
                    final int insertCount = ContactDb.insertContact(insertStmt, contact);
                    totalRowsIngested += insertCount;
                } else {
                    totalErrorRows++;
                    allValidationErrors.addAll(validationErrors);
                }
            }

            return new FileIngestResult(
                    file,
                    FileIngestResult.Status.OK,
                    lineNumber-1,
                    totalRowsIngested,
                    totalErrorRows,
                    null,
                    allValidationErrors
            );
        } catch (Exception e) {
            return new FileIngestResult(
                    file,
                    FileIngestResult.Status.ERROR,
                    lineNumber-1,
                    totalRowsIngested,
                    totalErrorRows,
                    e,
                    allValidationErrors
            );
        }
    }

}
