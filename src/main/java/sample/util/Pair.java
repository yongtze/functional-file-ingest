package sample.util;

public class Pair<L, R> {

    public static <L, R> Pair<L, R> of(final L first, final R second) {
        return new Pair<>(first, second);
    }

    private final L first;
    private final R second;

    public Pair(final L first, final R second) {
        this.first = first;
        this.second = second;
    }

    public L getFirst() {
        return first;
    }

    public R getSecond() {
        return second;
    }
}
