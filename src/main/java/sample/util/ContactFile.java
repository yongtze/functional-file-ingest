package sample.util;

import sample.model.Contact;
import sample.model.ValidationError;

import java.util.ArrayList;
import java.util.List;

public class ContactFile {

    public static Contact mapRow(final String[] tuple) {
        return new Contact(
                tuple[0],
                tuple[1],
                tuple[2],
                tuple[3],
                tuple[4],
                tuple[5],
                tuple[6],
                tuple[7],
                tuple[8],
                tuple[9],
                tuple[10]
        );
    }

    public static List<ValidationError> validateContact(final long lineNumber, final Contact contact) {
        final List<ValidationError> errors = new ArrayList<>();

        if (isBlank(contact.getFirstName()) || isBlank(contact.getLastName())) {
            errors.add(new ValidationError(lineNumber, "Name", "You must provide at least a First Name or Last Name."));
        }

        if (isBlank(contact.getCountryCode())) {
            errors.add(new ValidationError(lineNumber, "Country Code", "Country code is required."));
        }

        if (!isBlank(contact.getEmail()) && !isValidEmail(contact.getEmail())) {
            errors.add(new ValidationError(lineNumber, "Email", "Invalid email address: " + contact.getEmail()));
        }

        return errors;
    }

    private static boolean isBlank(final String value) {
        return value == null || value.trim().length() == 0;
    }

    private static boolean isValidEmail(final String email) {
        return email.matches(".+?@.+");
    }
}
