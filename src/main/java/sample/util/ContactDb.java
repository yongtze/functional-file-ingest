package sample.util;

import sample.model.Contact;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ContactDb {

    public static Connection createConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:postgresql://localhost:5432/contact", "contact-app", "contact-app");
    }

    public static PreparedStatement prepareInsertStatement(final Connection connection) throws SQLException {
        return connection.prepareStatement(
                "insert into contact.contacts (first_name, last_name, company, address_1, address_2, city, state, zip, country_code, email, phone) " +
                        "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
        );
    }

    public static int insertContact(final PreparedStatement insertStmt, final Contact contact) throws SQLException {
        insertStmt.setString(1, contact.getFirstName());
        insertStmt.setString(2, contact.getLastName());
        insertStmt.setString(3, contact.getCompany());
        insertStmt.setString(4, contact.getAddress1());
        insertStmt.setString(5, contact.getAddress2());
        insertStmt.setString(6, contact.getCity());
        insertStmt.setString(7, contact.getState());
        insertStmt.setString(8, contact.getZip());
        insertStmt.setString(9, contact.getCountryCode());
        insertStmt.setString(10, contact.getEmail());
        insertStmt.setString(11, contact.getPhone());
        return insertStmt.executeUpdate();
    }

}
