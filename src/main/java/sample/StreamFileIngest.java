package sample;

import sample.model.*;
import sample.util.ContactDb;
import sample.util.ContactFile;
import sample.util.Pair;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

public class StreamFileIngest {

    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Please specify the required filename parameter to run this program.");
            System.exit(1);
            return;
        }

        final FileIngestResult result = new StreamFileIngest().ingestFile(new File(args[0]));

        System.out.println("File");
    }

    private FileIngestResult ingestFile(final File file) {
        try (
                final Connection connection = ContactDb.createConnection();
                final PreparedStatement insertStmt = ContactDb.prepareInsertStatement(connection);
        ) {
            return indexedLine(file)
                .skip(1)    // Ignore the header line.
                .map(this::mapContact)
                .map(this::validateContact)
                .map(lineResult -> insertContact(insertStmt, lineResult))
                .reduce(new FileIngestResult(file),
                        (FileIngestResult fileResult, LineIngestResult lineResult) ->
                            fileResult.accumulate(lineResult.getValidationErrors(), lineResult.isInsertSucceed(), lineResult.getException()),
                        (a, b) -> a);
        } catch (Exception e) {
            return new FileIngestResult(file, FileIngestResult.Status.ERROR, 0, 0, 0, e, Collections.emptyList());
        }
    }

    private Stream<Pair<Long, String>> indexedLine(final File file) throws IOException {
        final AtomicLong lineNumber = new AtomicLong(0L);
        return Files.lines(file.toPath())
                .map(line -> Pair.of(lineNumber.incrementAndGet(), line));
    }

    private LineIngestResult mapContact(final Pair<Long, String> indexedLine) {
        final String[] tuple = indexedLine.getSecond().split("\\t");
        final Contact contact = ContactFile.mapRow(tuple);
        return new LineIngestResult(indexedLine.getFirst(), contact);
    }

    private LineIngestResult validateContact(final LineIngestResult lineResult) {
        final long lineNumber = lineResult.getLineNumber();
        final Contact contact = lineResult.getContact();
        return new LineIngestResult(lineNumber, contact, ContactFile.validateContact(lineNumber, contact));
    }

    private LineIngestResult insertContact(final PreparedStatement insertStmt, final LineIngestResult lineResult) {
        if (lineResult.hasValidationError()) {
            return lineResult;
        } else {
            try {
                ContactDb.insertContact(insertStmt, lineResult.getContact());
                return new LineIngestResult(lineResult.getLineNumber(), lineResult.getContact(), lineResult.getValidationErrors(), true, null);
            } catch (SQLException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }

}
