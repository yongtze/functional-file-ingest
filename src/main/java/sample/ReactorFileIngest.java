package sample;

import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import sample.model.Contact;
import sample.model.FileIngestResult;
import sample.model.LineIngestResult;
import sample.util.ContactDb;
import sample.util.ContactFile;

import java.io.File;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.stream.Stream;

public class ReactorFileIngest {

    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Please specify the required filename parameter to run this program.");
            System.exit(1);
            return;
        }

        final Mono<FileIngestResult> result$ = new ReactorFileIngest().ingestFile(new File(args[0]));

        final Disposable subscription = result$.subscribe(System.out::println);
        subscription.dispose();
    }

    public Mono<FileIngestResult> ingestFile(final File file) {
        return indexedLines(file)
                .skip(1)    // Ignore the header line.
                .map(this::mapContact)
                .map(this::validateContact)
                .transform(this::insertContact)
                .transform(this::handleException)
                .reduce(new FileIngestResult(file), (fileResult, lineResult) ->
                    fileResult.accumulate(
                        lineResult.getValidationErrors(),
                        lineResult.isInsertSucceed() != null && lineResult.isInsertSucceed(),
                        lineResult.getException()
                    )
                );
    }

    private Flux<Tuple2<Long, String>> indexedLines(final File file) {
        return Flux.using(
                () -> Files.lines(file.toPath()),
                (lines) -> Flux.fromStream(lines).index(),
                Stream::close
        );
    }

    private LineIngestResult mapContact(final Tuple2<Long, String> indexedLine) {
        return new LineIngestResult(
                indexedLine.getT1() + 1,
                ContactFile.mapRow(indexedLine.getT2().split("\\t"))
        );
    }

    private LineIngestResult validateContact(final LineIngestResult lineResult) {
        final long lineNumber = lineResult.getLineNumber();
        final Contact contact = lineResult.getContact();
        return new LineIngestResult(
                lineNumber,
                contact,
                ContactFile.validateContact(lineNumber, contact)
        );
    }

    private Flux<LineIngestResult> insertContact(final Flux<LineIngestResult> stream) {
        return Flux.using(
                this::initInsert,
                (resource) -> stream.map(lineResult -> insertContact(resource.getT2(), lineResult)),
                this::closeInsert
        );
    }

    private Tuple2<Connection, PreparedStatement> initInsert() {
        try {
            final Connection connection = ContactDb.createConnection();
            final PreparedStatement insertStmt = ContactDb.prepareInsertStatement(connection);
            return Tuples.of(connection, insertStmt);
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private LineIngestResult insertContact(final PreparedStatement insertStmt, final LineIngestResult lineResult) {
        if (lineResult.hasValidationError()) {
            return lineResult;
        } else {
            try {
                final int count = ContactDb.insertContact(insertStmt, lineResult.getContact());
                return new LineIngestResult(
                        lineResult.getLineNumber(),
                        lineResult.getContact(),
                        lineResult.getValidationErrors(),
                        count > 0,
                        null);
            } catch (SQLException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }

    private void closeInsert(final Tuple2<Connection, PreparedStatement> resource) {
        closeSilently(resource.getT2());
        closeSilently(resource.getT1());
    }

    private void closeSilently(final AutoCloseable resource) {
        try {
            resource.close();
        } catch (Exception ignored) {
        }
    }

    private Flux<LineIngestResult> handleException(final Flux<LineIngestResult> stream) {
        return stream.onErrorResume(ex -> Flux.just(new LineIngestResult(ex)));
    }

}
