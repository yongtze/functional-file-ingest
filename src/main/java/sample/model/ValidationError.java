package sample.model;

public class ValidationError {
    private final long lineNumber;
    private final String fieldName;
    private final String message;

    public ValidationError(final long lineNumber, final String fieldName, final String message) {
        this.lineNumber = lineNumber;
        this.fieldName = fieldName;
        this.message = message;
    }

    public long getLineNumber() {
        return lineNumber;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "ValidationError{" +
                "lineNumber=" + lineNumber +
                ", fieldName='" + fieldName + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
