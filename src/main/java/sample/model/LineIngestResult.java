package sample.model;

import java.util.List;

public class LineIngestResult {
    private final long lineNumber;
    private final Contact contact;
    private final List<ValidationError> validationErrors;
    private final Boolean insertSucceed;
    private final Throwable exception;

    public LineIngestResult(final long lineNumber, final Contact contact) {
        this(lineNumber, contact, null, null, null);
    }

    public LineIngestResult(final long lineNumber, final Contact contact, final List<ValidationError> validationErrors) {
        this(lineNumber, contact, validationErrors, null, null);
    }

    public LineIngestResult(final Throwable exception) {
        this(0, null, null, false, exception);
    }

    public LineIngestResult(final long lineNumber, final Contact contact, final List<ValidationError> validationErrors, final Boolean insertSucceed, final Throwable exception) {
        this.lineNumber = lineNumber;
        this.contact = contact;
        this.validationErrors = validationErrors;
        this.insertSucceed = insertSucceed;
        this.exception = exception;
    }

    public long getLineNumber() {
        return lineNumber;
    }

    public Contact getContact() {
        return contact;
    }

    public List<ValidationError> getValidationErrors() {
        return validationErrors;
    }

    public boolean hasValidationError() {
        return this.validationErrors != null && !this.validationErrors.isEmpty();
    }

    public Boolean isInsertSucceed() {
        return insertSucceed;
    }

    public Throwable getException() {
        return exception;
    }
}
