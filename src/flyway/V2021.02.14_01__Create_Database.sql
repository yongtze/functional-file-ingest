
create schema if not exists contact;

create table if not exists contact.contacts (
  id serial primary key,
  first_name text,
  last_name text,
  company text,
  address_1 text,
  address_2 text,
  city text,
  state text,
  zip text,
  country_code text,
  email text,
  phone text
);